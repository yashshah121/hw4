﻿using MenuItemDemos.Mode;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MenuItemDemos.Services
{
    public static class DataService
    {
        public static IList<Player> Players { get; set; }
        static DataService()
        {
            Players = new ObservableCollection<Player>() {
                new Player
                {
                    Name = "Kobe Bryant",
                    Position = "Shooting guard",
                    Team = "Los Angeles Lakers",
                    Url = "https://mir-s3-cdn-cf.behance.net/project_modules/disp/f3121a7457241.560abb7651116.png"
                },
                new Player
                {
                    Name = "LeBron James",
                    Position = "Power forward",
                    Team = "Cleveland Cavaliers",
                   
                },
                new Player{
                    Name ="Michael Jordan",

                    Position = "Shooting guard",
                    Team ="Chicago Bulls",
                    
                },
                new Player
                {
                    Name ="Magic Johnson",
                    Position = "Point guard",
                    Team = "Los Angeles Lakers",
                    
                },
                new Player
                {
                    Name = "Drazen Petrovic",
                    Position = "Shooting guard",
                    Team = "New Yersey nets"
                },

            };
        }
    }
}
