﻿using MenuItemDemos.Mode;
using MenuItemDemos.Services;
using System;

using Xamarin.Forms;

namespace MenuItemDemos
{
    public partial class MenuItemXamlPage : ContentPage
    {
        [Obsolete]
        public MenuItemXamlPage()
        {
            
            InitializeComponent();
            playerListView.ItemsSource = DataService.Players;
            var image = new Image { Source = "i1.png" };
            
        }

        private void OnDelete(object sender, System.EventArgs e)
        {
            var listItem = ((MenuItem)sender);
            var player = (Player)listItem.CommandParameter;

            DataService.Players.Remove(player);
        }
        private void OnEditClicked(object sender, System.EventArgs e)
        {
            var listItem = ((MenuItem)sender);
            var player = (Player)listItem.CommandParameter;

            DataService.Players.Remove(player);
        }
    }
}